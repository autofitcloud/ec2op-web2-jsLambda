## Deploying the python lambda functions

### Instruction set 0: Deprecated

Check below for newer instructions

```
pew new ec2op_osi
pip3 install -r requirements_dev.txt # using *_dev* so that netlify doesnt try to install these since they're only for the python backend that I manage myself (not through netlify)
```

Also requires (for private git urls) to generate an ssh key,
publish the public part in the s3 bucket `autofitcloud-com.autofitcloud.osi-public` (click on "make public" in s3)
and save the private part in another private s3 bucket.

Grant access to the private part of the key only to the lambda function `ec2op-osi` by attaching the following policy to the lambda function's role in IAM

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "s3:Get*",
                "s3:List*",
                "s3:HeadBucket"
            ],
            "Resource": [
                "arn:aws:s3:::<private bucketname>/id_rsa",
                "arn:aws:s3:::<private bucketname>"
            ]
        }
    ]
}
```


### Instruction set 1: DEPRECATED

Since netlify doesn't support python lambda functions, I need to custom-deploy my own lambda functions.

They're 2 functions ATM (2019-08-15): 

1. receive web POST and creates/edits item in dynamodb table. Deployed lambda function name: `ec2op-osi-webpost`
2. triggered from dynamodb table, runs optimization, saves results to a log file in the s3 bucket. Deployed lambda function name: `ec2op-osi`
    - make sure to create the folder "optimizations" in the bucket, as well as to have uploaded the lambda public ssh key 

To deploy them: `bash deploy_pylambda.sh`

Check inline in the code for the required S3 bucket name for the deployment.

PS: Eventhough I develop ec2op-cli under pew env `ec2op_dev`, this uses the editable python package.
It doesn't work with deploying the dependency packages to the python lambda function.
What I need to do is use the pew env `mvp.autofitcloud.com` which installs `ec2op-cli` from the `git+ssh` endpoint.

```
pew new mvp.autofitcloud.com
pip3 install -r requirements_dev.txt
```
The pew env name is also set up in the variable in `deploy_pylambda.sh`

Note that the lambda function `ec2op-osi` depends on the `git & ssh` lambda layer
referenced [here](http://www.devlo.io/git-in-aws-lambda.html) and [here](https://github.com/lambci/git-lambda-layer)
which makes sure that the executable `git` is available.

It also requires python 3.6 (not 3.7, for numpy sake?), max 2 minutes, and max memory 512 MB

Testing from curl to the published lambda function (via AWS API gateway)

```
curl --data '{"body": "{\"ec2op_method\": \"git-url\", \"git_url\": \"https://gitlab.com/shadiakiki1986/shadiakiki1986.aws.amazon.com-json\"}"}' <endpoint from AWS API Gateway>
curl --data '{"body": "{\"ec2op_method\": \"aws-credentials\", \"aws_credentials\": {\"aws_access_key_id\": \"ABC123\", \"aws_secret_access_key\": \"ABC123\", \"aws_region\": \"us-west-2\"}}"}' <endpoint from AWS API Gateway>

#-------------------
# pick up values from existing ~/.aws/credentials file
VAR_KEY=`grep aws_acce ~/.aws/credentials.bkp |cut -d\= -f2`
VAR_SEC=`grep aws_sec ~/.aws/credentials.bkp |cut -d\= -f2`
VAR_TOK=`grep aws_sess ~/.aws/credentials.bkp |cut -d\= -f2`

# build json
VAR_JSON=`cat <<EOF
{   "ec2op_method": "aws-credentials",
    "aws_access_key_id": "$VAR_KEY",
    "aws_secret_access_key": "$VAR_SEC",
    "aws_session_token": "$VAR_TOK",
    "AWS_DEFAULT_REGION": "us-west-2"
}
EOF
`
echo $VAR_JSON > testload.json

# send json with curl
curl --data @testload.json <endpoint>
```

Testing locally (directly with the python script before publishing it to lambda)

```
pew workon mvp.autofitcloud.com
python3 src/pylambda/ec2opcli_wrap.py git-url https://gitlab.com/shadiakiki1986/shadiakiki1986.aws.amazon.com-json
python3 src/pylambda/ec2opcli_wrap.py aws-credentials ABC123 ABC123 us-west-2

rm ~/.aws/credentials && \
     python3 src/pylambda/ec2opcli_wrap.py \
             aws-credentials \
             $VAR_KEY \
             $VAR_SEC \
             us-west-2 \
             $VAR_TOK
             
             
python3 -m src.pylambda.ec2opcli_webpost <event-body>
```

Environment variables required for lambda function `ec2op-osi`:

- `PATH` in the lambda function to be `/var/lang/bin:/usr/local/bin:/usr/bin/:/bin:/opt/bin:/var/task/bin` (notice the `/var/task/bin` appended).
    - Note: For copying executables to the pylambda function as well, eg `git-remote-aws+ec2`, 
- `GIT_SSH_COMMAND` env var to disable host key checking when git+ssh is used. Use `ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i /tmp/.ssh/id_rsa-osi.autofitcloud.com`
- `PYTHONPATH` env var to `/var/task:/var/runtime`
    - note `/var/runtime` is later so that the package uses botocore that I have installed here,
    - and not the default one in the lambda function


Environment variables required for lambda function `ec2op-osi-webpost`:

- `EC2OP_TOUCHRATE`: number of minutes to consider optimization data as "fresh" before triggering a refresh


Other permissions required for lambda functions

- `ec2op-osi-webpost`: write to dynamodb table `ec2op-osi`
- `ec2op-osi`: attach policy `AWSLambdaInvocation-DynamoDB` to the lambda role, write to s3 bucket `*public`, read from s3 bucket `*sshkeys`
 

Other notes

- Possibly enable the section in the `deploy_lambda...sh` script for the `bin` folder to be copied to the zip file.
    - Check if the `chmod +x` needs to be uncommented out.
- Remember that on 2019-08-09 I failed to get this to work, which prompted me to 
    - resort to the pre-computed git repo [ec2instances.info](https://gitlab.com/autofitcloud/www.ec2instances.info-ec2op)
    - But then again, on 2019-08-14 this worked again, but I kept it anyway because it's faster than re-computing each time

- Remember to enable "Lambda proxy integration" in the AWS API gateway under resources / post / integration request (and then "deploy API")
    - ([ref](https://stackoverflow.com/a/46122530/4126114))
 
 - the package was initially cloned from https://github.com/netlify/functions and stripped down.
    - Docs for netlify functions at https://www.netlify.com/docs/functions/
        - initially used for javascript function

The lambda functions are served behind AWS API GATEWAY

- enable cloudwatch logs integration with API gateway for easier debugging
