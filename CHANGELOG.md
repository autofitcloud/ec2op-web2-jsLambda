2019-08-20

- netlify front, enh:
  - move aws-credentials component completely out of index.js into its own js file
  - ditto aws-role
- netlify front, feat: aws-role to stop being "coming soon" and actually implemented like AwsCredsComp.js
- pylambda, enh: cli for role moved outside of try/except to show stacktrace
- general, feat: moving to the AutofitCloud AWS account rather than my personal AWS account
- pylambda, bugfix: rename `request_md5` to `request_hash`
- pylambda, feat: support external ID for aws role
- pylambda, bugfix: update ec2op-cli version in requirements + aws-role had wrong key for aws secret
- netlify front, enh: git-remote-aws no longer goes to separate api POST page


2019-08-19

- netlify front, enh: replace "WIP" with "coming soon" as recommended by Marco, and move the IAM role to the end


2019-08-16

- netlify front, enh: aws permissions updated
- netlify front, ENH: aesthetics like netlify announcement
- netlify front, FEAT: add aws role arn option
- pylambda, FEAT: implement aws role arn option
- pylambda, bugfix: webpost lambda should return a json stringified version with "message" as key
- pylambda, enh: factor out code for webpost lambda to class
- pylambda, enh: webpost lambda now only refreshes a row in 5-minute intervals maximal frequency
- pylambda, bugfix: optim lambda had a lot of bugs after having converted it to class without testing.. fixed
- pylambda, bugfix: decode the s3 object received
- pylambda, bugfix: transform the dyanmodb event to a postweb-similar event
- pylambda, enh: webpost return message is broken into dict of keys instead of one long string
- pylambda, bugfix: ignore REMOVE events for now
- pylambda, enh: allow re-trigger if s3 has error
- pylambda, bugfix: optim lambda no longer parses body as `parse_qs` but as json now
- netlify front, enh: finally settle on `region_name` from html form since will be passed into boto3 session constructor
- pylambda, bugfix: add url unquote, bring passe `parse_qs`, move stringio object into s3logman to fix issue with s3 logs being empty
- pylambda, enh: start using env var `EC2OP_TOUCHRATE` in webpost lambda to be able to control it without re-uploading to lambda
- pylambda, enh: show exact reasons for re-triggering calculation
- pylambda, bugfix: remove hashlib.md5 call from optim lambda (since already done in webpost lambda, and leading to different and inconsistent results)
    - also replaced with modifying the request transformation in the optim lambda to preserve the original request hash
- pylambda, bugfix: enable cors for webpost lambda
- pylambda, enh: save optimization results in optim lambda to "optimizations" folder in bucket (also modify the reader in webpost lambda to read from the folder)
- pylambda, bugfix: ec2op status was not really doing level=2
- pylambda, bugfix: an unquote on the form parameters causes the "+" in the aws secret to be converted to a " " (space). Typically, there are no spaces in it. Quick workaround implemented
- netlify front, feat: aws creds result is fetched with javascript and shown in html wtih vuejs
- netlify front, enh: mark the aws role as WIP



2019-08-15

- pylambda bugfix: change method of aws credentials to store them in `ec2op.yml`
  - because aws lambda already has the same-aws-account credentials
  - this method suits applying ec2op an *another* aws account
- pylambda enh: use `ec2op status --level=2` to avoid showing full report
- pylambda bugfix: instead of storing aws creds in `ec2op.yml`, just use a separate `boto3_session_config` file (`.json`) containing the `boto3_session_kwargs` dict
- pylambda FEAT: start working towards split architecture: 1. web request checks on s3 log. 2. runs actual optimization and updates s3 log
- pylambda feat: split out aws-credentials part of code to separate file, as well as git-remote-aws part, and utils.py
- pylambda feat: convert functions into classes
- pylambda feat: implement ec2op-cli wrapper that saves periodically to s3 file
- pylambda feat: add separate lambda function for receiving web posts, which inserts into ddb, which triggers the optimization lambda
- pylambda feat: deploy script now deploys both lambda functions
- pylambda feat: webpost lambda function is now functional .. TODO set up ddb table to trigger 2nd optimizer lambda upon insert


2019-08-14

- general, FEAT: rename repo to mvp.autofitcloud.com
- netlify back, ENH: minor fixup to footer of report output
- netlify front, ENH: split out git-remote-aws template html to separate file
- netlify front, FEAT: implement form of aws-credentials
- netlify back, ENH: implement aws-credentials passed
- pylambda, ENH: refactor function names for clarity
- pylambda, FEAT: implement aws-credentials passed
- netlify {front,back}, FEAT: add aws session token field + rename `aws_region` to `region`
- pylambda bugfix: finally got the git-remote-aws+ec2 executable to run in the lambda function
    - needed to move setting up the ssh key to the case of aws credentials also (and not just git private repos)
    - also needed to set `GIT_SSH_COMMAND` env var (documented in README) as well as re-enabling deployment of `git-remote-aws*` executables to lambda
- netlify front: move to directly using the pylambda function from the frontend because netlify back is limited to 10 seconds, and I could easily need more than that
- pylambda bugfix: lots of tweaks to get the lambda function to work as expected for the AWS credentials. Still doesnt work.


2019-08-13

- netlify front: integrate vuejs (in a very ugly way ATM) to use routes and switch between AWS credentials and git-remote-aws


2019-08-09

- pylambda, BUGFIX: since aws lambda is not seeing the git-remote-aws+ec2 helper, just clone the ec2instances.info data from my own gitlab public repo
- netlify back, FEAT: add git URL validation with axios and valid-url
- netlify back, FEAT: use axios to forward git URL to python lambda function (exposed via AWS API gateway)
- netlify front, ENH: add example URL, wider html view, drop unnecessary css in styles.css
- pylambda, FEAT: support private repos that grant access to a specific ssh public key
- netlify front: rename `requirements.txt` to  `requirements_dev.txt` to avoid netlify installing the dependencies (these are to be manually installed by me)
- pylambda, BUGFIX: consecutive calls to lambda will sustain the downloaded ssh key
- netlify back: cannot verify url for git@...git (private repos)
- pylambda FEAT: support for private repositories by allowing access to a public ssh key (and dropping paramiko)
- netlify back: sanitized a bit by getting the AWS API Gateway endpoint from an environment variable
- netlify front: aesthetics to front page
- netlify back: add footer text about Generated by ... and links


2019-08-08

- FEAT: first release
