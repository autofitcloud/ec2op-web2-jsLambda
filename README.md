# mvp.autofitcloud.com  [![Netlify Status](https://api.netlify.com/api/v1/badges/e552305a-78e7-498f-8f8d-c858ab9a0c5f/deploy-status)](https://app.netlify.com/sites/elegant-heisenberg-e9737f/deploys)

Internet-published wrapper of MVP of AutofitCloud (wraps ec2op-cli).

Web + Lambda for generating optimization report for AWS EC2 infrastructure

Deployed at https://mvp.autofitcloud.com/


## Components

This repo is comprised of:

- backend: python lambda functions
  - Deployed to my own AWS lambda function (exposed via AWS API Gateway) because netlify do not support python lambda ATM (2019-08-09)
  - 1st lambda function is to receive the frontend form's POST and save into dynamodb
  - 2nd lambda to fetch from dynamodb, pull relevant data, run optimization, and show results
- netlify front: html page frontend hosted on netlify.com
    - this is deployed directly by netlify


## Architecture

The general idea is:

- simple SPA allows user to input AWS IAM role, AWS IAM user credentials, or URL of git repository
- user clicks "optimize"
- user input goes to lambda function which generates the ec2op report using `ec2op-cli`
- Report is uploaded to an S3 bucket
- Lambda function returns the uploaded report to the user


And the general installation is

- Upload the app to netlify
- Upload the python lambda function to power an AWS REST API Gateway (with a single point-of-entry)
- Go to https://mvp.autofitcloud.com
- Optimize!


## Setup (netlify front)

To run the examples locally, here’s what you’ll need:

**System Requirements:**

* [git](https://git-scm.com)
* [NodeJS](nodejs.org) 8 or greater
* [yarn](yarnpkg.com)

`cd` into your local copy of the repository and run `yarn` or `npm install`

```
cd mvp.autofitcloud.com
yarn
```

Netlify automatically builds and deploys this through the `netlify.toml` file.
Note that the backend (javascript lambda function) requires setting the environment variable `PYLAM_URL`
to the AWS API Gateway URL endpoint of the python lambda function.

PS: The netlify backend (javascript functions) was initially used, and can be dropped now that I moved to python lambda functions.
Check [issue #5](https://gitlab.com/autofitcloud/mvp.autofitcloud.com/issues/5)



## Running the examples (netlify front)

Requires running the "python back"

```
yarn start
```

This will start the client server on http://localhost:8080, and the netlify-lambda server on http://localhost:9000.

[netlify-lambda](https://github.com/netlify/netlify-lambda) isn’t required to deploy Lambda functions to Netlify, but it offers some handy features out of the box that make it quicker to get started, like the local dev server and nice defaults for transpiling and bundling functions in production.

The client server is configured to proxy `/.netlify` requests to the Lambda server (see [webpack.client.js](webpack.client.js)). This is the same behavior the site has when it’s deployed to Netlify.


## Deploying the python lambda functions

The below instructions were re-written after migrating from my personal AWS account to the AutofitCloud AWS account.

For the older instructions, check `INSTALL_DEPRECATED.md`

1. Create s3 buckets

```
# pseudocode using aws cli
aws s3 create mvp-sshkeys # keep the checkbox checked for "Block all access"
aws s3 create mvp-public # uncheck the box "Block all access"
aws s3 cp path/to/id_rsa s3://mvp-sshkeys/ # RSA private key to be used by lambda function to git clone private repos
aws s3 cp path/to/id_rsa.pub s3://mvp-sshkeys/ # RSA public key (counter-part of above private key), to be shared publicly
aws s3 mkdir s3://mvp-public/optimizations # folder where logs of optimizations are saved
```

2. Create lambda functions

```
# pseudocode
aws lambda create mvp-ec2opcli \
    python3.6 \
    --entrypoint pylambda.ec2opcli_wrap.lambda_handler \
    --memory 512MB \
    --timeout 120s \
    --env PATH="/var/lang/bin:/usr/local/bin:/usr/bin/:/bin:/opt/bin:/var/task/bin" \
    --env GIT_SSH_COMMAND="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i /tmp/.ssh/id_rsa-osi.autofitcloud.com" \
    --env PYTHONPATH="/var/task:/var/runtime" \
    --attach-policy AmazonS3FullAccess \
    --attach-policy AmazonDynamoDBFullAccess \
    --attach-policy AWSLambdaInvocation-DynamoDB \
    --attach-policy sts_assumerole \
    --attach-policy AmazonEC2ReadOnlyAccess \
    --attach-policy CloudWatchReadOnlyAccess \
    --attach-layer arn:aws:lambda:eu-central-1:553035198032:layer:git:6
    
# Note in above ^^^^^^^^^^ that sts_assumerole is an in-line policy that I set up manually for the STS service, AssumeRole action, and applied to all resources.
# Also note that AmazonEC2ReadOnlyAccess and CloudWatchReadOnlyAccess above are given from the assumed role

    
aws lambda create mvp-webpost \
    python3.6 \
    --entrypoint pylambda.ec2opcli_webpost.lambda_handler \
    --memory 128MB \
    --timeout 10s \
    --env EC2OP_TOUCHRATE=60 \
    --attach-policy AmazonS3ReadOnlyAccess  \
    --attach-policy AmazonDynamoDBFullAccess 
```

3. Create dynamodb table

```
aws dynamodb create mvp-ec2op \
    --primary-key request_hash \
    --add-trigger lambda:mvp-ec2opcli,batch-size:1
```

4. Edit and upload lambda functions

Editing the variables

```
src/pylambda/ec2opcli_webpost.py DDB_TABLE
src/pylambda/utils.py BUCKET_SSHKEY,BUCKET_PUBLIC
```

Upload

```
bash deploy_pylambda.sh
```

5. Create API Gateway

```
aws api-gateway create \
    --type REST \
    --template-from NEW \
    --name mvp-ec2op \
    --description "API behind mvp.autofitcloud.com" \
    --create-method POST,Lambda,UseLambdaProxyIntegration,lambda:mvp-webpost
```

Copy the new endpoint to the netlify front-endpoint in `src/common.js`



## License

Check `LICENSE.md`
