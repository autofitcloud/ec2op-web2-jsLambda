#!/usr/bin/env bash
# Deploy the python lambda function
#
# Usage: bash deploy_pylambda.sh
#
# Ref:
#   https://stackoverflow.com/questions/44208895/aws-lambda-python-function-with-pandas-dependency
#   https://github.com/joarleymoraes/aws_lambda_deploy/blob/master/scripts/deploy_lambda.sh
#   https://joarleymoraes.com/hassle-free-python-lambda-deployment/

set -e

lambda_project_home="$(pwd)"
dist_dir_name="dist_pylambda"
# proj_file_names=("src"  "conf" "data")
proj_file_names=("src/pylambda")
pew_env="mvp.autofitcloud.com" # do not use ec2op_dev which uses `pip install . -e` for ec2op-cli
env_path_root="/home/ubuntu/.local/share/virtualenvs/$pew_env"

# After resorting to my own git repo of ec2instances.info, no longer need to copy the git-remote-aws+ec2 remote helper to this lambda function anymore
# For other lambda functions, it may be necessary to include executables, which would be done by adding more folders to the below
# Edit: after adding AWS credentials as an option, need the executables from git-remote-aws again
# env_path_names=("lib/python3.6/site-packages/*" "bin/git-remote-aws*")
#
# The libs_cffi* is required to avoid the below error on AWS lambda
# Unable to import module 'pylambda.ec2opcli_wrap': libffi-ae16d830.so.6.0.4: cannot open shared object file: No such file or directory
# Ref https://aws.amazon.com/fr/blogs/compute/scheduling-ssh-jobs-using-aws-lambda/
#
# Edit: paramiko no longer needed, hence ffi lib not needed. Check note in ec2opcli_wrap.py on why paramiko was no longer needed
# env_path_names=("lib/python3.6/site-packages/*" "lib/python3.6/site-packages/.libs_cffi_backend/")
#
# Going back to uploading git-remote-aws* executables after addition of AWS credentials option
# env_path_names=("lib/python3.6/site-packages/*")
env_path_names=("lib/python3.6/site-packages/*")
env_path_bins=("bin/git-remote-aws*"  "bin/ec2op")

# n_libs_dir_name="native_libs" # replaced by "bin" above
deploy_bundle_name="lambda_bundle_ec2op_osi.zip"


# 2019-08-20 update bucket name where the code is uploaded to be deployed to lambda
# s3_deploy_bucket="crow-v0-lambda"
s3_deploy_bucket="mvp-lambda.autofitcloud.com"

s3_deploy_key=${deploy_bundle_name}

aws_cli_profile=""


dist_path=${lambda_project_home}/${dist_dir_name}

echo "Cleaning up dist dir ..."
rm -rf ${dist_path}/*

echo "Adding source files ..."

for sf in "${proj_file_names[@]}"
do
    proj_path=${lambda_project_home}/${sf}
    cp -rf ${proj_path} ${dist_path}
done

echo "Adding pip libs ..."

for sf in "${env_path_names[@]}"
do
    proj_path=${env_path_root}/${sf}
    
    # only needed for executables, but not sure how to condition it in the bash script
    # Not using the executables at the moment anyway, so ignoring this for now
    # chmod +x ${proj_path}
    
    cp -rf ${proj_path} ${dist_path}
done

echo "Adding executables ..."
mkdir ${dist_path}/bin
for sf in "${env_path_bins[@]}"
do
    proj_path=${env_path_root}/${sf}
    
    # only needed for executables, but not sure how to condition it in the bash script
    # Not using the executables at the moment anyway, so ignoring this for now
    # chmod +x ${proj_path}
    
    cp -rf ${proj_path} ${dist_path}/bin
done

# fix the hashbang for executables from
#     #!/home/ubuntu/.local/share/virtualenvs/mvp.autofitcloud.com/bin/python3
# to 
#     #!/var/lang/bin/python
sed -i "s/#\!.*/#\!\/var\/lang\/bin\/python/g" ${dist_path}/bin/*


echo "Create deployment package folder ..."

cd ${dist_path}

zip -q -r ${deploy_bundle_name} .

mv ${deploy_bundle_name} ${lambda_project_home}/

cd -

echo "Uploading deployment package to S3 ..."

deploy_bundle_path=${lambda_project_home}/${deploy_bundle_name}
aws s3 cp ${deploy_bundle_path} s3://${s3_deploy_bucket}/${s3_deploy_key} \
    && echo "Successful s3 Upload" || (echo "Failed s3 upload" && exit 1)

echo "Updating Lambda functions ..."

function awslambdaWrap() {
    lambda_function_name="$1"
    
    aws lambda update-function-code \
        --function-name ${lambda_function_name} \
        --s3-bucket ${s3_deploy_bucket} \
        --s3-key ${s3_deploy_key} \
        --publish \
        && echo "lambda Deployment completed successfully: " $lambda_function_name \
        || (echo "Failed lambda" $lambda_function_name && exit 1)
}


# Edit 2019-08-20 update lambda function names
# awslambdaWrap "ec2op-osi"
# awslambdaWrap "ec2op-osi-webpost"
awslambdaWrap "mvp-ec2opcli"
awslambdaWrap "mvp-webpost"

echo `date` ": complete"
