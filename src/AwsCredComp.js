var POST_URL = require('./common.js').POST_URL

var AwsCredHtml = `
<div>
    <h4>Use AWS credentials for pulling data and running optimization.</h4>
    <p>Same as in <pre style="display:inline">~/.aws/credentials</pre>. Check the required minimum permissions below for more details.</p>
    
    <div v-if="!ref_num">
    <!-- https://alligator.io/vuejs/vue-form-handling/ -->
    <form @submit.prevent="handleSubmitAwsCreds">
      <p>
        <input type="hidden" v-model="ec2op_method" >

        <label>
          AWS Access Key ID
          &nbsp;
          <input type="text" v-model="input.aws_access_key_id" style="width: 50em;" placeholder="e.g. ABC123DEF456">
        </label>

        <label>
          AWS Secret Access Key
          &nbsp;
          <input type="text" v-model="input.aws_secret_access_key" style="width: 50em;" placeholder="e.g. ABCdef123GHI+456">
        </label>

        <label>
          AWS Region
          &nbsp;
          <input type="text" v-model="input.region_name" style="width: 50em;" placeholder="e.g. us-west-2">
        </label>

        <label>
          AWS Session Token (optional)
          &nbsp;
          <input type="text" v-model="input.aws_session_token" style="width: 50em;" placeholder="e.g. Abc123">
        </label>

      </p>
      <p><button type="submit">Optimize!</button></p>
      <p>
          Examples:
          <pre class="language-bash">
cat ~/.aws/credentials
[default]
aws_access_key_id = ABC123DEF456
aws_secret_access_key = ABCdef123GHI+456
aws_session_token=Abcdef
region = us-west-2
          </pre>
      </p>
    </form>



    <br/><hr/><br/>

    <h4>Minimum IAM policy:</h4>
    <p>The credentials provided above should be assigned to a IAM role with a policy that has the following minimum permissions:</p>
    <pre class="language-bash">
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ec2:DescribeInstances",
                "ec2:DescribeInstanceStatus",
                "cloudwatch:GetMetricData",
                "cloudwatch:GetMetricStatistics"
            ],
            "Resource": "*"
        }
    ]
}
    </pre>
    </div>


    <div v-else>
      <h3>
        Optimization Results
        &nbsp;
        <button v-on:click="handleSubmitAwsCreds()">Refresh</button>
        &nbsp;
        <button v-on:click="reset()">New optimization</button>
      </h3>
      <table class='table'>
      <tr class="spaceUnder"><td>Reference number</td><td>{{ref_num}}</td></tr>
      <tr class="spaceUnder"><td>Refresh Status</td><td>{{ddb_trigger}}</td></tr>
      <tr class="spaceUnder"><td>Optimization error</td><td>{{s3_error?s3_error:'None'}}</td></tr>
      <tr class="spaceUnder"><td>Optimization result</td><td><pre>{{s3_content}}</pre></td></tr>
      </table>
    </div>

        
</div>
`;


var AwsCredComp = {
  template: AwsCredHtml,
  data: function() {
    return {
      ec2op_method: 'aws-credentials',
      input: {
        aws_access_key_id: '',
        aws_secret_access_key: '',
        region_name: '',
        aws_session_token: ''
      },
      // output: {
        ref_num: '',
        s3_error: '',
        s3_content: '',
        ddb_trigger: '',
      //},
      error: ''
    };
  },
  methods: {
    reset: function() {
      this.ref_num = '';
      this.s3_error = '';
      this.s3_content = '';
      this.ddb_trigger = '';
    },
    handleSubmitAwsCreds: function() {
      var vm = this;
      // use axis with urlencoded form
      const params = new URLSearchParams();
      params.append('ec2op_method', vm.ec2op_method);
      params.append('aws_access_key_id', vm.input.aws_access_key_id);
      params.append('aws_secret_access_key', vm.input.aws_secret_access_key);
      params.append('region_name', vm.input.region_name);
      params.append('aws_session_token', vm.input.aws_session_token);
      console.log(params.toString());

			axios.post(POST_URL, params)
				.then(function (response) {
					// console.log(response);
          vm.ref_num = response.data.ref_num;
          vm.s3_error = response.data.s3_error;
          vm.s3_content = response.data.s3_content;
          vm.ddb_trigger = response.data.ddb_trigger;
          //vm.$forceUpdate();
				})
				.catch(function (error) {
					console.log(error);
          vm.error = error;
				});
    }
  }
}
module.exports = AwsCredComp
