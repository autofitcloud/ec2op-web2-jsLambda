var POST_URL = require('./common.js').POST_URL

var AwsRoleHtml = `
<div>
    <h4>Use AWS IAM Role for pulling data and running optimization.</h4>
    <p>Check the instructions below for more details.</p>

    
    <div v-if="!ref_num">
    <!-- https://alligator.io/vuejs/vue-form-handling/ -->
    <form @submit.prevent="handleSubmitAwsRole">
      <p>
        <label>
          AWS Role ARN
          &nbsp;
          <input type="text" v-model="input.role_arn" style="width: 50em;" placeholder="e.g. arn:aws:iam::<account>:role/<name>">
        </label>

        <br/>

        <label>
          External ID (optional, recommended)
          &nbsp;
          <input type="text" v-model="input.external_id" style="width: 50em;" placeholder="e.g. 'abc123' (random string)">
        </label>

        <br/>

        <label>
          Region name
          &nbsp;
          <input type="text" v-model="input.region_name" style="width: 50em;" placeholder="e.g. 'us-west-2' for Oregon">
        </label>

      </p>
      <p><button type="submit">Optimize!</button></p>
      <p>
        The provided Role should have the attached policies <pre class='language-bash'>AmazonEC2ReadOnlyAccess, CloudWatchReadOnlyAccess</pre>
      </p>

    </form>

    <br/><hr/><br/>

    <h4>To create a new AWS Role via the AWS management console</h4>
    <ol>
      <li>Click <a href="https://console.aws.amazon.com/iam/home#/roles$new?step=type">Create new role</a> in the IAM service</li>
      <li>Select type of trusted entity: Another AWS account</li>
      <li>Enter Account ID: 974668457921 <br/> <small>This is the AWS account number for <a href="https://autofitcloud.com">AutofitCloud</a></small></li>
      <li>Click Next: Permissions</li>
      <li>Select policy: AmazonEC2ReadOnlyAccess, CloudWatchReadOnlyAccess</li>
      <li>Click Next: Tags</li>
      <li>Click Next: Review</li>
      <li>Enter Role name: autofitCloud-ec2-readonly</li>
      <li>Enter Role description: Allows autofitcloud.com to get EC2 descriptions, EC2 statuses, Cloudwatch metrics</li>
      <li>Click Create role</li>
    </ol>
    </div>


    <div v-else>
      <h3>
        Optimization Results
        &nbsp;
        <button v-on:click="handleSubmitAwsRole()">Refresh</button>
        &nbsp;
        <button v-on:click="reset()">New optimization</button>
      </h3>
      <table class='table'>
      <tr class="spaceUnder"><td>Reference number</td><td>{{ref_num}}</td></tr>
      <tr class="spaceUnder"><td>Refresh Status</td><td>{{ddb_trigger}}</td></tr>
      <tr class="spaceUnder"><td>Optimization error</td><td>{{s3_error?s3_error:'None'}}</td></tr>
      <tr class="spaceUnder"><td>Optimization result</td><td><pre>{{s3_content}}</pre></td></tr>
      </table>
    </div>

</div>
`;


var AwsRoleComp = {
  template: AwsRoleHtml,
  data: function() {
    return {
      ec2op_method: 'aws-role',
      input: {
        role_arn: '',
        external_id: '',
        region_name: ''
      },
      // output: {
        ref_num: '',
        s3_error: '',
        s3_content: '',
        ddb_trigger: '',
      //},
      error: ''
    };
  },
  methods: {
    reset: function() {
      this.ref_num = '';
      this.s3_error = '';
      this.s3_content = '';
      this.ddb_trigger = '';
    },
    handleSubmitAwsRole: function() {
      var vm = this;
      // use axis with urlencoded form
      const params = new URLSearchParams();
      params.append('ec2op_method', vm.ec2op_method);
      params.append('role_arn', vm.input.role_arn);
      params.append('external_id', vm.input.external_id);
      params.append('region_name', vm.input.region_name);
      console.log(params.toString());

			axios.post(POST_URL, params)
				.then(function (response) {
					// console.log(response);
          vm.ref_num = response.data.ref_num;
          vm.s3_error = response.data.s3_error;
          vm.s3_content = response.data.s3_content;
          vm.ddb_trigger = response.data.ddb_trigger;
          //vm.$forceUpdate();
				})
				.catch(function (error) {
					console.log(error);
          vm.error = error;
				});
    }
  }
}

module.exports = AwsRoleComp
