// useful var
// Edit 2019-08-20 change the endpoint from the one in my personal AWS account to the one in the AutofitCloud AWS account
// const POST_URL = "https://71qmo5hxx8.execute-api.us-west-2.amazonaws.com/production/";
const POST_URL = "https://mfiiq1fe4m.execute-api.eu-central-1.amazonaws.com/production/";

module.exports = {
  POST_URL: POST_URL
}
