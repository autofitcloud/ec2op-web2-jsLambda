var POST_URL = require('./common.js').POST_URL

var GraHtml = `
<div>
    <h4>Use git-remote-aws to avoid giving out AWS credentials for optimization.</h4>
    <p>
      Pull your data with <a href="https://gitlab.com/autofitcloud/git-remote-aws">git-remote-aws</a>,
      push it to a git repository,
      and paste the URL below
      to get your optimization report.
    </p>
    <p>Check the full example below for more details.</p>

    
    <div v-if="!ref_num">
    <!-- https://alligator.io/vuejs/vue-form-handling/ -->
    <form @submit.prevent="handleSubmitGra">
      <p>
        <label>
          Git URL
          &nbsp;
          <input type="text" v-model="input.git_url" style="width: 50em;" placeholder="URL to git remote">
        </label>
      </p>
      <p><button type="submit">Optimize!</button></p>
      <p>
          Examples:
          <pre class="language-url">
https://gitlab.com/shadiakiki1986/shadiakiki1986.aws.amazon.com-json.git
git@gitlab.com:shadiakiki1986/shadiakiki1986.aws.amazon.com-json-private.git
git+ssh://git@gitlab.com:shadiakiki1986/shadiakiki1986.aws.amazon.com-json-private.git
          </pre>
      </p>
    </form>
    </div>
    

    <div v-else>
      <h3>
        Optimization Results
        &nbsp;
        <button v-on:click="handleSubmitGra()">Refresh</button>
        &nbsp;
        <button v-on:click="reset()">New optimization</button>
      </h3>
      <table class='table'>
      <tr class="spaceUnder"><td>Reference number</td><td>{{ref_num}}</td></tr>
      <tr class="spaceUnder"><td>Refresh Status</td><td>{{ddb_trigger}}</td></tr>
      <tr class="spaceUnder"><td>Optimization error</td><td>{{s3_error?s3_error:'None'}}</td></tr>
      <tr class="spaceUnder"><td>Optimization result</td><td><pre>{{s3_content}}</pre></td></tr>
      </table>
    </div>

    <br/><hr/><br/>
    
    <h4>Full example:</h4>
    <ol>
      <li>
        Install <a href="https://aws.amazon.com/cli/">awscli</a> and configure it on your own server.
        <pre class="language-bash">
apt-get install python3 python3-pip
pip install awscli
aws configure
        </pre>
      </li>
      <li>
        Install <a href="https://gitlab.com/autofitcloud/git-remote-aws">git-remote-aws</a>.
        <pre class="language-bash">
apt-get install git
pip install git-remote-aws
        </pre>
      </li>
      <li>
        Pull your AWS EC2 data with <i>git-remote-aws</i> just like you would pull a git repository:
        <pre class="language-bash">
TMPDIR=\`mktemp -d\`
cd $TMPDIR

git init

git remote add ec2_descInstances  aws+ec2::/describe-instances?fulldata=false
git remote add cw_getMetricData   aws+cw::/get-metric-data
git remote add cw_listMetrics     aws+cw::/list-metrics

git fetch --all
        </pre>
      </li>
      <li>
        Check the downloaded data with terminal tools like cat and jq.
        
        <pre class="language-bash">
> tree
.
└── aws.amazon.com
    └── us-west-2
        ├── cw_getMetricData
        │   ├── CPUUtilization - daily - 90 days - Average
        │   │   ├── i-02432bc7.json
        │   │   ├── i-069a7808addd143c7.json
        │   │   ├── i-08c802de5accc1e89.json
        .
        .
        .
        └── ec2_describeInstances
            ├── i-02432bc7.json
            ├── i-069a7808addd143c7.json
            ├── i-08c802de5accc1e89.json
            .
            .
            .
</pre>

      </li>
      <li>
        Push the data to a git repository.
        <pre class="language-bash">
git add aws.amazon.com
git commit -m 'first commit'

git remote add origin git@gitlab.com:username/reponame.git
git push -u origin master
        </pre>
      </li>
      <li>For private git repositories to be cloned over ssh, grant access to <a href="https://s3-us-west-2.amazonaws.com/autofitcloud-com.autofitcloud.osi-public/id_rsa.pub">this ssh public key</a>.</li>
      <li>
        <p>Paste the git remote URL below (the same URL that goes to <i>git clone ...</i>).</p>
        <p>Here are examples of a public repository cloned over https and a private one cloned over ssh</p>
        
        <pre class="language-url">
https://gitlab.com/shadiakiki1986/shadiakiki1986.aws.amazon.com-json.git
git@gitlab.com:shadiakiki1986/shadiakiki1986.aws.amazon.com-json-private.git
git+ssh://git@gitlab.com:shadiakiki1986/shadiakiki1986.aws.amazon.com-json-private.git
</pre>
      </article>

      </li>
    </ol>
</div>
`;


var GraComp = {
  template: GraHtml,
  data: function() {
    return {
      ec2op_method: 'git-url',
      input: {
        git_url: ''
      },
      // output: {
        ref_num: '',
        s3_error: '',
        s3_content: '',
        ddb_trigger: '',
      //},
      error: ''
    };
  },
  methods: {
    reset: function() {
      this.ref_num = '';
      this.s3_error = '';
      this.s3_content = '';
      this.ddb_trigger = '';
    },
    handleSubmitGra: function() {
      var vm = this;
      // use axis with urlencoded form
      const params = new URLSearchParams();
      params.append('ec2op_method', vm.ec2op_method);
      params.append('git_url', vm.input.git_url);
      console.log(params.toString());

			axios.post(POST_URL, params)
				.then(function (response) {
					// console.log(response);
          vm.ref_num = response.data.ref_num;
          vm.s3_error = response.data.s3_error;
          vm.s3_content = response.data.s3_content;
          vm.ddb_trigger = response.data.ddb_trigger;
          //vm.$forceUpdate();
				})
				.catch(function (error) {
					console.log(error);
          vm.error = error;
				});
    }
  }
}

module.exports = GraComp
