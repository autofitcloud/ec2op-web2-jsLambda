from .utils import run_cmd, clone_awscat
import os
import json
import tempfile
from click.testing import CliRunner
from ec2op.core.cli import cli as ec2op_cli
from git import Repo as GitRepo


class AwscredMan:
    
    def __init__(self, aws_credentials, s3lm, logger):
        self.aws_credentials = aws_credentials
        self.s3lm = s3lm
        self.logger = logger
    
    def setup_awsconfig(self, boto3_session_config):
        # > aws configure set help
        # aws configure set aws_access_key_id default_access_key
        # aws configure set aws_secret_access_key default_secret_key
        # aws configure set default.region us-west-2
        # Just pasted the function here instead of importing # from .ec2opcli_wrap2 import run_cmd
        # self.logger.info(run_cmd(["aws", "configure", "set", "aws_access_key_id", self.aws_credentials["aws_access_key_id"]]))
        # self.logger.info(run_cmd(["aws", "configure", "set", "aws_secret_access_key", self.aws_credentials["aws_secret_access_key"]]))
        # self.logger.info(run_cmd(["aws", "configure", "set", "default.region", self.aws_credentials["region"]]))
        
        # above didnt work, so will just write out the plain file
        # Note, write to /tmp/.aws instead of ~/.aws
        # aws_unexp = "~/.aws/credentials"
        # aws_fn = os.path.expanduser(aws_unexp)
        #aws_dir = "/tmp/.aws/"
        #aws_fn = os.path.join(aws_dir, "credentials")
        #if os.path.isfile(aws_fn):
        #    # raise Exception("File already exists: %s. Aborting"%aws_fn)
        #    os.remove(aws_fn)
        #     
        #os.makedirs(aws_dir, exist_ok=True)
        #with open(aws_fn, 'w') as fh:
        #     fh.write("[default]\n")
        #     for k in self.aws_credentials:
        #         if k.lower()=='ec2op_method': continue
        #         v = self.aws_credentials[k]
        #         if k.upper()=='region_name': k='region' # force the change
        #         if v is None: continue
        #         fh.write("%s=%s\n"%(k, v))
        #         
        ## https://boto3.amazonaws.com/v1/documentation/api/latest/guide/configuration.html#environment-variable-configuration
        ## os.environ['AWS_SHARED_CREDENTIALS_FILE'] = aws_fn
        #self.logger.info('AWS_SHARED_CREDENTIALS_FILE=', os.environ['AWS_SHARED_CREDENTIALS_FILE'])
        #run_cmd(['cat /tmp/.aws/credentials'])
        
        # above doesn't work on lambda where ~/.aws is restrcited
        # use env vars instead
        # Above doesn't work well with git-remote-aws+ec2 which depends on "region" in credentials
        # Going back to .aws/credentials but in /tmp instead of ~
        #self.logger.info("AWS creds")
        # self.logger.info(self.aws_credentials) # FIXME
        #for k in self.aws_credentials:
        #    v = self.aws_credentials[k]
        #    if v is None: continue
        #    os.environ[k.upper()] = v
        #    self.logger.info(k.upper(), '=', v)
        
        # Save AWS credentials in the ec2op.yml file
        aws_cr2 = {}
        for k in self.aws_credentials:
            if k.lower()=='ec2op_method': continue # skip
            v = self.aws_credentials[k]
            if v is None: continue
            aws_cr2[k.lower()] = v
            # self.logger.info(k.upper(), '=', v)
    
        self.logger.info("saving boto3 config to json file: %s"%boto3_session_config.name)
        # self.logger.info(json.dumps(aws_cr2))
        with open(boto3_session_config.name, 'w') as fh:
          json.dump(aws_cr2, fh)

    
    
    def handle_awscredentials_l1(self):
    
        # run full ec2op command set
        # use the same way of testing click apps
        # https://click.palletsprojects.com/en/7.x/testing/
        # delete tempfile dirs after completion
        with tempfile.TemporaryDirectory() as repo_dir_root:
          with tempfile.TemporaryDirectory() as repo_dir_awscat:
            with tempfile.NamedTemporaryFile(suffix='.json') as boto3_session_config:
              self.logger.info("Workdir: %s"%repo_dir_root)
              self.s3lm.write_logs()
              self.setup_awsconfig(boto3_session_config) # setup aws credentials in ec2op
              self.s3lm.write_logs()
              result_status = self.handle_awscredentials_l2(repo_dir_root, repo_dir_awscat, boto3_session_config)
              self.s3lm.write_logs()
              
        return result_status
    
              
    def handle_awscredentials_l2(self, repo_dir_root, repo_dir_awscat, boto3_session_config):
            # repo_dir_root = tempfile.mkdtemp() # for persistent folder
            # repo_dir_awscat = tempfile.mkdtemp() # for persistent folder
            
            os.chdir(repo_dir_root)
            runner = CliRunner()
            
            self.logger.info('ec2op init')
            result_init = runner.invoke(ec2op_cli, ['init', '--boto3_session_config', boto3_session_config.name])
            if result_init.exit_code!=0:
                raise Exception(result_init.output)
                
            self.s3lm.write_logs()
    
    
            # self.logger.info('ec2op pull core --all')
            # result_core = runner.invoke(ec2op_cli, ['pull', 'core', '--all'])
            # if result_core.exit_code!=0:
            #     # result_core.output would only be "Pulling aws+ec2::/describe-instances?fulldata=false" even on error
            #     raise Exception("Failed: ec2op pull core --all")
            
            # show remotes
            # result_remote = runner.invoke(ec2op_cli, ['remote'])
            # self.logger.info(result_remote.output)
            
            # replace above `ec2op pull core --all` with `cd core && git fetch --all` for a clearer error message
            # for the sake of the MVP, just pull the minimum
            gr_core =  GitRepo("core")
            #for r in gr_core.remotes:
            #    r.fetch()
            # gr_core.remotes['r0_aws_ec2_descInstances'].fetch()
            # gr_core.remotes['aws_cw_getMetricData'].fetch()
            
            # fixme try the subprocess method because gitpython doesnt forward stderr properly
            repo_dir_core = os.path.join(repo_dir_root, 'core')
            os.chdir(repo_dir_core)
            run_cmd(['git fetch r0_aws_ec2_descInstances'])
            self.s3lm.write_logs()
            run_cmd(['git fetch aws_cw_getMetricData'])
            self.s3lm.write_logs()
            os.chdir(repo_dir_root)
            
            # clone catalog from my own pre-computed gitlab repo instead of live ec2instances.info endpoint
            clone_awscat(repo_dir_awscat, repo_dir_core)
            self.s3lm.write_logs()
    
            self.logger.info('ec2op pull optimizer r4_optimizer_optimize')
            #result_optim = runner.invoke(ec2op_cli, ['pull', 'optimizer', 'r4_optimizer_optimize'])
            #if result_optim.exit_code!=0:
            #    raise Exception("Failed: ec2op pull optimizer r4_optimizer_optimize")
            # Edit 2019-08-14 the click runner is not outputing anything if the ec2op pull fails. Trying better luck with the subprocess method
            # self.logger.info("PYTHONPATH")
            # self.logger.info(os.environ['PYTHONPATH'])
            # run_cmd(['ec2op', 'pull', 'optimizer', 'r4_optimizer_optimize'])
            run_cmd(['ec2op pull optimizer r4_optimizer_optimize'])
            self.s3lm.write_logs()
    
            self.logger.info('ec2op status --level=2')
            result_status = runner.invoke(ec2op_cli, ['status', '--level', '2'])
            if result_status.exit_code!=0:
                raise Exception("Failed: ec2op status")
    
            self.s3lm.write_logs()
            
            return result_status.output
