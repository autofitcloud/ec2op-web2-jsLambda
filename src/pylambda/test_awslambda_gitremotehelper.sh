#!/bin/bash

set -ex

echo `date`

TMPDIR=`mktemp -d`
cd $TMPDIR
git init
# git remote add r1_aws_ec2catalog aws+ec2::/catalog
# git fetch r1_aws_ec2catalog
git remote add r1_aws_ec2catalog git@gitlab.com:autofitcloud/www.ec2instances.info-ec2op.git
git pull r1_aws_ec2catalog master
ls $TMPDIR

###############

# cp git-remote-aws* executables to /opt/bin just in case it's necessary to work with the git-lambda-layer
# cp /var/task/git-remote-aws+ec2 /opt/bin
which python
python --version
which python3
python3 --version
git-remote-aws+ec2 || echo "I know" # test that it runs

# Test aws+ec2 git remote helper
git remote add r0_aws_ec2_descInstances  aws+ec2::/describe-instances?fulldata=false
git fetch r0_aws_ec2_descInstances
tree