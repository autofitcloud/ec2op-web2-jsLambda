from click.testing import CliRunner
from ec2op.core.cli import cli as ec2op_cli
import boto3
import os
import tempfile
from git import Repo as GitRepo
import shutil
from .utils import clone_awscat


class GiturlMan:
    
    def __init__(self, giturl_core, sshkey_fn, s3lm, logger):
        self.giturl_core = giturl_core
        self.sshkey_fn = sshkey_fn
        self.s3lm = s3lm
        self.logger = logger
        
        
    # core function
    def handle_giturl_l2(self, repo_dir_root, repo_dir_awscat):
        # cd temp dir
        self.logger.info("Working in root: %s. Also, awscat: %s"%(repo_dir_root, repo_dir_awscat))
        os.chdir(repo_dir_root)
        self.s3lm.write_logs()
    
        # use the same way of testing click apps
        # https://click.palletsprojects.com/en/7.x/testing/
        runner = CliRunner()
        result_init = runner.invoke(ec2op_cli, ['init'])
        self.logger.info(result_init)
        self.s3lm.write_logs()
    
        # clone and replace the original default "core"
        repo_dir_core = os.path.join(repo_dir_root, 'core')
        shutil.rmtree(repo_dir_core)
        
        # use the specfic ssh key that was downloaded
        # https://stackoverflow.com/questions/28291909/gitpython-and-ssh-keys#28292493
        # Also disable host key checking here (since paramiko method below failed on lambda with error "Could not create directory '/home/sbx_user1051/.ssh'.")
        git_ssh_cmd = 'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i %s ' % self.sshkey_fn
        # with Git().custom_environment(GIT_SSH=git_ssh_cmd):
        gr_core = GitRepo.clone_from(self.giturl_core, repo_dir_core, env={"GIT_SSH_COMMAND": git_ssh_cmd})
        self.s3lm.write_logs()
        
        # duck-typing to verify that the repo is indeed exported with git-remote-aws
        if not os.path.exists(os.path.join(repo_dir_core, "aws.amazon.com")):
            raise Exception("Git repo %s is not exported by git-remote-aws. Aborting."%self.giturl_core)
        
        # add remote for catalog and fetch
        # Couldn't get the git-remote-aws+ec2 to be recognized in AWS lambda by the GitPython call to git subprocess
        # so as a workaround, which is also more efficient, just clone the pre-run repo
        # gr_awscat.create_remote("r1_aws_ec2catalog", "aws+ec2::/catalog")
        # gr_awscat.remotes.r1_aws_ec2catalog.fetch()
        
        # Clone the pre-run rpeo with ec2instances.info json files, stripped to the ec2op needs
        # and copy it to a subfolder in the ec2op repo root
        clone_awscat(repo_dir_awscat, repo_dir_core)
        self.s3lm.write_logs()
    
        # optimize
        result_optim = runner.invoke(ec2op_cli, ['pull', 'optimizer', 'r4_optimizer_optimize'])
        self.logger.info(result_optim)
        result_status = runner.invoke(ec2op_cli, ['status'])
        self.logger.info(result_status)
        self.s3lm.write_logs()
    
        return result_status.output
        
    
    
    
    def handle_giturl_l1(self):
        # delete tempfile dirs after completion
        with tempfile.TemporaryDirectory() as repo_dir_root:
            with tempfile.TemporaryDirectory() as repo_dir_awscat:
                ret_msg = self.handle_giturl_l2(repo_dir_root, repo_dir_awscat)
        
        # avoid deleting tempfile dirs after execution (useful for debugging)
        # repo_dir_root = tempfile.mkdtemp()
        # repo_dir_awscat = tempfile.mkdtemp()
        # ret_msg = handle_giturl_l2(repo_dir_root, repo_dir_awscat)
    
        return ret_msg
    