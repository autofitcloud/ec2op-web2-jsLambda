# for click locale .. http://click.palletsprojects.com/en/7.x/python3/
from gitRemoteAws.utils import mysetlocale
mysetlocale()

#----------------

import sys
import tempfile
from urllib.parse import parse_qs
import logging
import io

logger = logging.getLogger("ec2op-osi-cliWrap")

#-------------------------------------------------
from .awsroleMan import AwsroleMan
from .awscredMan import AwscredMan
from .giturlMan import GiturlMan
from .utils import setup_sshkey, S3logMan

#------------------------

def prep_helpers(request_hash):
    # prepare s3log manager
    # set up logging to StringIO for periodic snapshotting to s3
    # https://stackoverflow.com/a/51070892/4126114
    s3lm = S3logMan(request_hash)
    
    # set lowest level in logger
    logger.setLevel(logging.DEBUG)

    # add handlers
    sh1 = logging.StreamHandler(s3lm.log_stringio)
    sh1.setLevel(logging.INFO) # this goes to the end-user ATM
    logger.addHandler(sh1)
    
    sh2 = logging.StreamHandler(sys.stdout)
    sh2.setLevel(logging.DEBUG) # this goes to cloudwatch ATM
    logger.addHandler(sh2)

    
    # setup the ssh key so that private repos can be cloned
    # Note that this is necessary for public repos too which use git+ssh
    sshkey_fn = setup_sshkey()

    return s3lm, sshkey_fn


#---------------------
# Set up handler function for aws lambda

import json
def lambda_handler(event, context):
    logging.warning("Received event")
    logging.warning(event)
    
    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>
    # shave the dynamodb-triggered event for simplification
    
    # take only 1 record at a time (remember to set this in the dynamodb trigger of the lambda function too)
    event = event['Records'][0]
    
    logging.warning("Received event name: %s", event['eventName'])
    
    if event['eventName']=='REMOVE':
        # TODO implement dropping the file from s3?
        return

    # only focus on insert and modify events
    if not event['eventName'] in ('INSERT', 'MODIFY'):
        return
    
    event = event['dynamodb']['NewImage']
    # <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    
    # event = json.loads(event)
    # event = {'body': event}
    logging.warning("Transformed event to")
    logging.warning(event)
    
    # use request's hash for the s3 key at which to save the logs
    # get helper vars
    s3lm, sshkey_fn = prep_helpers(event['request_hash']['S'])
    
    # parse json string
    # edit 2019-08-14 this is no longer a json load in "body" but rather a direct POST with payload
    # event['bodyParsed'] = json.loads(event['body'])
    # https://stackoverflow.com/a/46122530/4126114
    # Also, this info typically never contains a space, but I've observed that if the aws secret has "+" in it initially,
    # this automatically gets converted to a space with the "unquote" below. The fastest workaround is just to replace it
    # back.
    eventParsed = parse_qs(event['event']['S'])
    for k in eventParsed: eventParsed[k] = eventParsed[k][0].replace(' ', '+') # just take 1st entry of each list
    # edit 2019-08-16 event body is json again now
    # Edit: Nope!
    # eventParsed = json.loads(event['body'])
    
    try:
        ec2op_method = eventParsed['ec2op_method'] # if key not available, will raise Exception
        
        logging.warning("ec2op_method = %s"%ec2op_method)

        if ec2op_method=="git-url":
          try:
              giturl_core = eventParsed['git_url'] # if key not available, will raise Exception
          except KeyError as error:
              raise Exception("Missing key in lambda event: %s"%(str(error)))
            
          logging.warning("trigger git-url manager: start")
          gm = GiturlMan(giturl_core, sshkey_fn, s3lm, logger)
          ret_msg = gm.handle_giturl_l1()
          logging.warning("trigger git-url manager: end")


        elif ec2op_method=="aws-credentials":
            
          # Edit 2019-08-14 no longer using an intermediate key ['aws_credentials'] # if key not available, will raise Exception
          logging.warning("trigger aws-creds manager: start")
          aws_credentials = eventParsed
          acm1 = AwscredMan(aws_credentials, s3lm, logger)
          ret_msg = acm1.handle_awscredentials_l1()
          logging.warning("trigger aws-creds manager: end")


        elif ec2op_method=="aws-role":
            
          logging.warning("trigger aws-role manager: start")
          arm = AwsroleMan(eventParsed['role_arn'], eventParsed['external_id'], s3lm, logger)
          aws_credentials = arm.get_temporary_credentials()
          aws_credentials['region_name'] = eventParsed['region_name']
          acm2 = AwscredMan(aws_credentials, s3lm, logger)
          ret_msg = acm2.handle_awscredentials_l1()
          logging.warning("trigger aws-role manager: start")


        else:
          raise Exception("Unsupported ec2op method %s"%ec2op_method)

    except Exception as error:
        logging.warning("Error: %s"%str(error))
        ret_msg = str(error)
        
    # some art
    logger.info("")
    logger.info("")
    logger.info("-------------------------------")
    logger.info("-------------------------------")
    logger.info("  ---------   ---------   ---")
    logger.info("  ---     -   ---    --   ---")
    logger.info("  ---         ---         ---")
    logger.info("  -------     ---         ---")
    logger.info("  ---         ---         ---")
    logger.info("  ---     -   ---    --   ---")
    logger.info("  ---------   ---------   ---")
    logger.info("-------------------------------")
    logger.info("-------------------------------")
    logger.info("")
    logger.info("")
    
    # save to s3
    logging.warning("Saving result to s3: start")
    logger.info(ret_msg)
    s3lm.write_logs()
    logging.warning("Saving result to s3: end")


#---------------------
# Set up CLI for local testing

def cli_dummyData():
    request_hash = "foo-bar"
    s3lm, sshkey_fn = prep_helpers(request_hash)
    return request_hash, s3lm, sshkey_fn
    
    

import click
@click.group()
def cli():
    pass



@click.command(name="git-url")
@click.argument('git_url')
def cli_gra(git_url):
    # via lambda_handler
    # event = {'body': json.dumps({'git_url': giturl_core})}
    # result_status = lambda_handler(event=event, context=None)
    # print(json.loads(result_status['body'])['message'])
    
    request_hash, s3lm, sshkey_fn = cli_dummyData()

    # direct
    gm = GiturlMan(git_url, sshkey_fn, s3lm, logger)
    result_status = gm.handle_giturl_l1()
    
    # common end        
    print(result_status)
    



@click.command(name="aws-credentials")
@click.argument('keyid')
@click.argument('secret')
@click.argument('region')
@click.argument('token', default=None)
def cli_awscred(keyid, secret, region, token):
    request_hash, s3lm, sshkey_fn = cli_dummyData()

    logger.info("Start cli_awscred") # do after cli_dummyData for setLevel to run

    aws_credentials = {
        "aws_access_key_id": keyid,
        "aws_secret_access_key": secret,
        "region_name": region,
        "aws_session_token": token
    }
    try:
        logger.info("Start aws cred man")
        am = AwscredMan(aws_credentials, s3lm, logger)
        result_status = am.handle_awscredentials_l1()

    except Exception as error:
        print(str(error))
        sys.exit(1)

    # common end        
    print(result_status)



@click.command(name="aws-role")
@click.argument('role_arn')
@click.argument('external_id')
@click.argument('region_name')
def cli_awsrole(role_arn, external_id, region_name):
    request_hash, s3lm, sshkey_fn = cli_dummyData()

    arm = AwsroleMan(role_arn, external_id, s3lm, logger)
    aws_credentials = arm.get_temporary_credentials()
    aws_credentials['region_name'] = region_name
    acm = AwscredMan(aws_credentials, s3lm, logger)
    result_status = acm.handle_awscredentials_l1()

    # common end        
    print(result_status)


cli.add_command(cli_gra)
cli.add_command(cli_awscred)
cli.add_command(cli_awsrole)




if __name__ == '__main__':
    def show_usage():
        print("Usage:")
        print("\tpython3 ec2opcli_wrap.py git-url https://gitlab.com/shadiakiki1986/shadiakiki1986.aws.amazon.com-json")
        print("\tpython3 ec2opcli_wrap.py aws-credentials <key-id> <secret> <region> <token>")
        print("\tpython3 ec2opcli_wrap.py aws-role <role-arn> <external-id> <region-name>")
        print("\n")

    show_usage() # always show usage for info
    cli()
