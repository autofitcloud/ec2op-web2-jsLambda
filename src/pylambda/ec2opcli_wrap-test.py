import os
import sys
import json
import boto3


def run_cmd(cmnd):
    # Copied from
    # https://stackoverflow.com/a/16198668/4126114
    print("runcmd")
    print(cmnd)
    import subprocess
    try:
        output = subprocess.check_output(
            cmnd, stderr=subprocess.STDOUT, shell=True, timeout=30,
            universal_newlines=True)
        print("done run cmd")
        print(output)
    except subprocess.CalledProcessError as exc:
        return "Status : FAIL. %s. %s."%(exc.returncode, exc.output)
    else:
        return "Output: \n{}\n".format(output)



def setup_sshkey():
    # generate ~/.ssh file structure, delete the generated key, download the one from the "secure" s3 bucket
    # ssh-keygen -b 2048 -t rsa -f /tmp/sshkey -q -N ""
    # Ref 2: https://unix.stackexchange.com/a/69318/312018
    # import subprocess
    # from .ec2opcli_wrap2 import run_cmd
    # run_cmd(["ssh-keygen", "-b", "2048", "-t", "rsa", "-f", "/tmp/sshkey", "-q", "-N"])
    
    # No need for ssh-keygen above, since the key would be deleted anyway
    # Cannot use os.path.expanduser("~/.ssh/") in AWS lambda (readonly system, except /tmp)
    dir_dotssh = "/tmp/.ssh/"
    target_keyname = os.path.join(dir_dotssh, 'id_rsa-osi.autofitcloud.com') # instead of the default 'id_rsa'
    
    # If the key is not already there, download
    # Note that it seems that consecutive runs of the lambda function might sustain the downloaded key from the earlier call
    if os.path.isfile(target_keyname):
        # chmod 600 key .. run this anyway just in case it's necessary
        os.chmod(target_keyname, 0o600)

        return target_keyname
    
    # mkdir ~/.ssh && chmod 700 ~/.ssh
    os.makedirs(dir_dotssh, mode=0o700, exist_ok=True)

    # download private key from secure s3 bucket
    # Ref 1: https://aws.amazon.com/fr/blogs/compute/scheduling-ssh-jobs-using-aws-lambda/ (search keyname.pem)
    s3_client = boto3.client('s3')
    s3_client.download_file('autofitcloud-com.autofitcloud.osi-sshkey','id_rsa', target_keyname)
    
    # chmod 600 key
    os.chmod(target_keyname, 0o600)
    
    # use paramiko to autoadd missing hostkey
    # Should I just get the proper host keys? Not sure how this plays with automation.
    # I can do it for github.com, gitlab.com, but what about private repositories?
    # Turns out that this requires being able to create ~/.ssh folder, but lambda aws is readonly
    # so I'm just appending "StrictHostKeyChecking=no" to the ssh key above
    # c = paramiko.SSHClient()
    # c.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    
    return target_keyname


def lambda_handler(event, context):
    # FIXME Debugging
    print("lambda")
    
    setup_sshkey()
    
    return {
        'statusCode': 400,
        'body': json.dumps({
            #'message': os.environ['PATH']
            #'message': subprocess.check_output(["ls", "-al", "/var/task/"]).decode("utf-8")
            # 'message': subprocess.check_output(["which", "git-remote-aws+ec2"]).decode("utf-8")
            # 'message': run_cmd(["ls"])
            
            # for some reason, bash needs to be in the same item with its argument
            'message': "\n\n".join([
                run_cmd(["pwd"]),
                # run_cmd(["ls"]),
                run_cmd(["find . -name pylambda"]),
                run_cmd(["which ssh"]),
                run_cmd(["which git-remote-aws+ec2"]),
                run_cmd(["bash pylambda/test_awslambda_gitremotehelper.sh"])
            ])
        })
    }
    


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Usage: python3 ec2opcli_wrap.py https://gitlab.com/shadiakiki1986/shadiakiki1986.aws.amazon.com-json")
        sys.exit(1)
        
    git_url = sys.argv[1]
    event = {'body': json.dumps({'git_url': git_url})}
    result_status = lambda_handler(event=event, context=None)
    
    # show results
    print(json.loads(result_status['body'])['message'])