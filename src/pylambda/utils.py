import os
import boto3
import subprocess
import shutil


# Edit 2019-08-20 update bucket name to match with the one I created on the AutofitCloud AWS account
# BUCKET_SSHKEY = 'autofitcloud-com.autofitcloud.osi-sshkey'
BUCKET_SSHKEY = 'mvp-s3-sshkey.autofitcloud.com'

def setup_sshkey():

    # generate ~/.ssh file structure, delete the generated key, download the one from the "secure" s3 bucket
    # ssh-keygen -b 2048 -t rsa -f /tmp/sshkey -q -N ""
    # Ref 2: https://unix.stackexchange.com/a/69318/312018
    # import subprocess
    # from .ec2opcli_wrap2 import run_cmd
    # run_cmd(["ssh-keygen", "-b", "2048", "-t", "rsa", "-f", "/tmp/sshkey", "-q", "-N"])
    
    # No need for ssh-keygen above, since the key would be deleted anyway
    # Cannot use os.path.expanduser("~/.ssh/") in AWS lambda (readonly system, except /tmp)
    dir_dotssh = "/tmp/.ssh/"
    target_keyname = os.path.join(dir_dotssh, 'id_rsa-osi.autofitcloud.com') # instead of the default 'id_rsa'
    
    # If the key is not already there, download
    # Note that it seems that consecutive runs of the lambda function might sustain the downloaded key from the earlier call
    if os.path.isfile(target_keyname):
        # chmod 600 key .. run this anyway just in case it's necessary
        os.chmod(target_keyname, 0o600)

        return target_keyname
    
    # mkdir ~/.ssh && chmod 700 ~/.ssh
    os.makedirs(dir_dotssh, mode=0o700, exist_ok=True)

    # download private key from secure s3 bucket
    # Ref 1: https://aws.amazon.com/fr/blogs/compute/scheduling-ssh-jobs-using-aws-lambda/ (search keyname.pem)
    s3_client = boto3.client('s3')
    s3_client.download_file(BUCKET_SSHKEY,'id_rsa', target_keyname)
    
    # chmod 600 key
    os.chmod(target_keyname, 0o600)
    
    # use paramiko to autoadd missing hostkey
    # Should I just get the proper host keys? Not sure how this plays with automation.
    # I can do it for github.com, gitlab.com, but what about private repositories?
    # Turns out that this requires being able to create ~/.ssh folder, but lambda aws is readonly
    # so I'm just appending "StrictHostKeyChecking=no" to the ssh key above
    # c = paramiko.SSHClient()
    # c.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    
    return target_keyname
    
    

# Edit 2019-08-14 commented out run_cmd after no longer using it because of move to env var
def run_cmd(cmnd):
    # Copied from
    # https://stackoverflow.com/a/16198668/4126114
    print("run_cmd")
    print(cmnd)
    try:
        output = subprocess.check_output(
            cmnd, stderr=subprocess.STDOUT, shell=True, timeout=30,
            universal_newlines=True)
        print("complete")
        print(output)
    except subprocess.CalledProcessError as exc:
        raise Exception("Status : FAIL. Return code: %i. Command: %s. Error: %s."%(exc.returncode, " ".join(cmnd), exc.output))
    else:
        return "Output: \n{}\n".format(output)
        
        
        
def clone_awscat(repo_dir_awscat, repo_dir_core):
    # Clone the pre-run rpeo with ec2instances.info json files, stripped to the ec2op needs
    # and copy it to a subfolder in the ec2op repo root
    # giturl_awscat = 'git@gitlab.com:autofitcloud/www.ec2instances.info-ec2op.git'
    giturl_awscat = 'https://gitlab.com/autofitcloud/www.ec2instances.info-ec2op.git'
    from git import Repo as GitRepo
    gr_awscat = GitRepo.clone_from(giturl_awscat, repo_dir_awscat)
    gr_awscat.remotes.origin.pull()
    shutil.copytree(
        os.path.join(repo_dir_awscat, "www.ec2instances.info"),
        os.path.join(repo_dir_core, "www.ec2instances.info")
    )



# Edit 2019-08-20 update bucket name to match with the one I created on the AutofitCloud AWS account
# https://stackoverflow.com/a/51070892/4126114
# BUCKET_PUBLIC = 'autofitcloud-com.autofitcloud.osi-public'
BUCKET_PUBLIC = 'mvp-s3-public.autofitcloud.com'

import io
class S3logMan:
    def __init__(self, request_md5):
        self.s3 = boto3.client("s3")
        self.request_md5 = request_md5
        self.log_stringio = io.StringIO()

    def write_logs(self):
        # print("log_stringio >>> %s <<<"%self.log_stringio.getvalue())
        self.s3.put_object(Body=self.log_stringio.getvalue(), Bucket=BUCKET_PUBLIC, Key="optimizations/"+self.request_md5)

# Round datetime object to 10-min before it
# https://stackoverflow.com/a/3464000/4126114
import datetime as dt
def roundDatetime(tm, n_min):
    return tm - dt.timedelta(minutes=tm.minute % n_min, seconds=tm.second, microseconds=tm.microsecond)
    
