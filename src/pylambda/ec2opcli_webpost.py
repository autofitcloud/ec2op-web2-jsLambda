import sys
import hashlib
import logging
import boto3
from .utils import BUCKET_PUBLIC, roundDatetime
from botocore.exceptions import ClientError
from urllib.parse import unquote
import os

#-------------------------------------------------

# Set up handler function for aws lambda
import datetime as dt
import json

# name of dynamodb table to use
# Edit 2019-08-20 update dynamodb table name after moving to AutofitCloud aws account
# DDB_TABLE = 'ec2op-osi'
DDB_TABLE = 'mvp-ec2op'

# minutes between re-calculations if the same record is requested
TOUCH_RATE = int(os.getenv('EC2OP_TOUCHRATE', '60'))

# format to use for datetime conversion to/from string
MY_DT_STRFORMAT = '%Y-%m-%d %H:%M:%S.%f'

class WebpostMan:
    def __init__(self, event):
        self.event = event
    
        # md5 hash of request for identifying the request
        # Used for specifying the key to save to s3
        # Note that this will give different values on different machines
        # because the md5 salt will be different (randomly generated per machine)
        self.request_hash = hashlib.md5(event['body'].encode()).hexdigest()
        
        # set up logging to StringIO for periodic snapshotting to s3
        # https://stackoverflow.com/a/51070892/4126114
        self.logger = logging.getLogger("ec2op-osi-webpost")
        self.logger.setLevel(logging.DEBUG) # FIXME
        ch = logging.StreamHandler(sys.stdout) # FIXME
        self.logger.addHandler(ch) # FIXME
    
        # boto3 clients
        self.client_s3 = boto3.client("s3")
        self.client_ddb = boto3.client("dynamodb")
        
        # timestamp now
        self.dt_now_obj = dt.datetime.now()
        self.dt_now_str = dt.datetime.strftime(self.dt_now_obj, MY_DT_STRFORMAT)
        
        # variables holding info that will go in the ret_msg in the end
        self.s3_error = None
        self.s3_content = None
        self.ddb_found = None
        self.ddb_trigger = None

        self.logger.debug("webpostman.init: end")

        
    def deal_all(self):
        self.logger.debug("webpostman.deal_all: start")
        self.handle_s3()
        self.handle_ddb_general()
        self.logger.debug("webpostman.deal_all: end")
        
        
    def handle_s3(self):
        try:
            ret_obj = self.client_s3.get_object(Bucket=BUCKET_PUBLIC, Key="optimizations/"+self.request_hash)
            ret_msg = ret_obj["Body"].read().decode()
            
            if len(ret_msg)>0:
                self.logger.debug("webpostman.deal_all: found s3 and not empty")
                self.s3_content = ret_msg
                return
            
            self.logger.debug("webpostman.deal_all: found s3 but empty")
            self.s3_error = "Empty s3 file"
            return
        
        except ClientError as ex1:
            if ex1.response['Error']['Code'] != 'NoSuchKey':
                raise ex1
                
            self.s3_error = 'Log file not found in S3.'
            return

                
    def handle_ddb_general(self):
            # check if already kicked off a new calculation
            try:
                item_get = self.client_ddb.get_item(TableName=DDB_TABLE, Key={'request_hash': {'S': self.request_hash}})
                
                if not 'Item' in item_get:
                    return self.handle_notInDdb()
                    
                return self.handle_ddb_found(item_get)
                
            except ClientError as ex2:
                if ex2.response['Error']['Code'] != 'NoSuchKey':
                    raise ex2
                    
                return self.handle_notInDdb()

                
    def handle_ddb_found(self, item_get):
                self.ddb_found = True
                
                # check if already touched the row within the last "bin"
                if 'dt_last' in item_get['Item']:
                    # datetime of last touch of row
                    # eg 2019-08-16 09:19:53.983681
                    dt_last_str = item_get['Item']['dt_last']['S']
                    dt_last_obj = dt.datetime.strptime(dt_last_str, MY_DT_STRFORMAT)
                    dt_last_floor = roundDatetime(dt_last_obj, n_min=TOUCH_RATE)
                    dt_now_floor = roundDatetime(self.dt_now_obj, n_min=TOUCH_RATE)
                    if dt_now_floor == dt_last_floor:
                        if not self.s3_error:
                            self.ddb_trigger = "Calculation not re-triggered (already triggered within the last %i minutes on %s, and no s3 error).\n"%(TOUCH_RATE, dt_last_str)
                            self.logger.debug("webpostman.deal_all: no re-trigger")
                            return
                
                # touch the row in the dynamodb table to trigger refresh by other lambda function optimizer
                item_put = item_get['Item'].copy()
                item_put['dt_last'] = {'S': self.dt_now_str}
                self.client_ddb.put_item(TableName=DDB_TABLE, Item=item_put)
                    
                trigger_reasons = ["s3 error" if self.s3_error else None, "data not fresh" if dt_now_floor != dt_last_floor else None]
                trigger_reasons = [x for x in trigger_reasons if x]
                self.ddb_trigger = "Calculation re-triggered (%s)"%(", ".join(trigger_reasons))
                self.logger.debug("webpostman.deal_all: yes re-trigger")
                return



    def handle_notInDdb(self):
        self.ddb_found = False
        
        # kick off a new calculation
        item = {
            'request_hash': {'S': self.request_hash}, 
            # body is a queryset: a=1&b=2..., that is urlencoded (hence the need for unquote)
            'event': {'S': unquote(self.event['body']) },
            'dt_created': {'S': self.dt_now_str},
            'dt_last': {'S': self.dt_now_str},
        }
        self.client_ddb.put_item(TableName=DDB_TABLE, Item=item)
        
        self.ddb_trigger = "Calculation kicked off.\n"
        self.logger.debug("webpostman.deal_all: handle not in ddb: end")
        return


def lambda_handler(event, context):
    wm = WebpostMan(event)
    try:
        wm.deal_all()
        
        # assemble into a single dict
        ret_msg = {
            'ref_num': wm.request_hash,
            's3_error': wm.s3_error,
            's3_content': wm.s3_content,
            'ddb_trigger': wm.ddb_trigger
        }
        ret_code = 200

    except Exception as error:
        ret_code = 500
        ret_msg = "Internal server error: %s"%str(error)
    

    # some logging
    wm.logger.debug("Return code:")
    wm.logger.debug(ret_code)
    wm.logger.debug("Return body:")
    wm.logger.debug(json.dumps(ret_msg))
    
    # return
    return {
        'statusCode': ret_code,
        # enable CORS
        # https://docs.aws.amazon.com/apigateway/latest/developerguide/how-to-cors.html
        'headers': { "Access-Control-Allow-Origin": "*" },
        'body': json.dumps(ret_msg)
    }

#---------------------
# Set up CLI for local testing

import click

@click.command()
@click.argument('event_body')
def hello(event_body):
    res = lambda_handler({'body': event_body}, None)
    print(res)



if __name__ == '__main__':
    def show_usage():
        print("Usage:")
        print("\tpython3 -m src.pylambda.ec2opcli_webpost <event-body>")
        print("\tpython3 -m src.pylambda.ec2opcli_webpost '{...}'")
        print("\n")

    show_usage() # always show usage for info
    hello()
