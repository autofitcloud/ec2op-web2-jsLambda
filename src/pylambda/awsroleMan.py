import boto3

class AwsroleMan:
    
    def __init__(self, aws_rolearn, external_id, s3lm, logger):
        self.aws_rolearn = aws_rolearn
        self.external_id = external_id
        self.s3lm = s3lm
        self.logger = logger
    

    def get_temporary_credentials(self):
        # https://aws.amazon.com/premiumsupport/knowledge-center/lambda-function-assume-iam-role/
        # import os
        # for k in os.environ: print(k, ': ', os.environ[k])
        
        #self.logger.debug("will assume_role as follows")
        #self.logger.debug("RoleArn:")
        #self.logger.debug(self.aws_rolearn)
        #self.logger.debug("ExternalId:")
        #self.logger.debug(self.external_id)
        
        sts_connection = boto3.client('sts')
        acct_b = sts_connection.assume_role(
                RoleArn=self.aws_rolearn,
                ExternalId=self.external_id,
                RoleSessionName="cross_acct_lambda"
        )
				
        self.logger.info("got credentials for boto3 config")
        # self.logger.error(acct_b)

        return {
            'aws_access_key_id': acct_b['Credentials']['AccessKeyId'],
            'aws_secret_access_key': acct_b['Credentials']['SecretAccessKey'],
            'aws_session_token': acct_b['Credentials']['SessionToken']
        }

